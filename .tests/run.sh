#!/bin/bash

# Initialize counters
passed=0
failed=0

# Run each test script in the current directory
for test_script in test_*; do
    # Skip non-executable files
    [ -x "$test_script" ] || continue

    # Run the test script
    ./"$test_script"

    # Check if the test script has a temporary log file
    if [ -f temp_log ]; then
        # Check if the test script has any failed tests
        if grep -q 'FAILED' temp_log; then
            # Display failed tests log
            echo "Failed tests in $test_script:"
            cat temp_log
            echo
            ((failed++))
        else
            ((passed++))
        fi
        # Remove temporary log file
        rm temp_log
    else
        echo "No log file found for $test_script"
    fi
done

# Display test results
echo "Tests Passed: $passed"
echo "Tests Failed: $failed"
