<div align = center>

# ScriptOS
<br><br>
&ensp;[<kbd> <br> Install <br> </kbd>](#Installation)&ensp;
&ensp;[<kbd> <br> Post Install <br> </kbd>](#Post-Installation)&ensp;
&ensp;[<kbd> <br> Keybindings <br> </kbd>](#Keybindings)&ensp;
&ensp;[<kbd> <br> Packages <br> </kbd>](#Packages)&ensp;
&ensp;[<kbd> <br> Themes <br> </kbd>](#Themes)&ensp;
<br><br></div>

"Optionated" Arch Linux installer, allowing for a seemsless automatic install while opening up the install commands, in order to guide the customisation of your installation. The structure of the project has been kept clean and offers a good base for advance users to customise scripts quickly at code level instead of installer.

The goal of this Project is to bring the ArchWiki to life, by intergrating the wiki and community wisdom and scripts into an arch installer, in order to setup a deeply customised and configured machine for modern hardware on ArchLinux OS from first boot. Focusing on being secure, stable, with optimised hardware and themed highly visually appealing Desktop managers. 

The goal is not to become another distro. ArchLinux is already highly maintained and distributed.

### Currently Opinionated. Maybe "Optionated" in the future
[arch-secure-boot](https://github.com/maximbaz/arch-secure-boot) <br>
    - Most secure and fastest approuch to booting system (no bootloader).<br>
    - Snapshot restore via bios shell.<br>
    - Luks2<br>
    - unified images<br>
    - one password decrypt<br>

No dual boot (maybe it can be configured). VM Fully setup near Host hardware perfromance.<br>
Only hyprland DM has a setup script. You can select no for hyprland install and add packages in package manager options for your DM.<br>

### "Optionated" Features
The following has been integrated and "optionated":

- [ ] [OS: Curently only Arch Linux for desktop/ laptop]()
- [ ] [Desktops managers: Hyprland(hyprWorld) (Gnome, KDE, i3... please help... in progress...)]()
- [ ] [Auto detected for Desktops and Laptops and seperatly configured]()
- [ ] [AMD, Intel, Nvidia CPU's and GPU's auto detected and configured]()
- [ ] [Detection of old and modern Nvidia GPU's]()
- [ ] [System wide configuration of Nvidia GPU's for hyprland support]()
- [ ] [Prime-run guided config to select apps to offload to dGPU balanced performance]()
- [ ] [xrun guided config to select apps to bypass dGPU directly to dedicated XSever (Max performance)]()
- [ ] [System partition creation, based on SUSE Linux BTFRS scheme]()
- [ ] [Secureboot, Unfied, no bootloader, dual boot, BTFRS, LUK2, Snapshots]()
- [ ] [Snapper configured]()
- [ ] [Hyprland configured and themed (HyprWorld)]()
- [ ] [Installation Package Manager]()
- [ ] [Restic backup script. With local or remote setup options]()
- [ ] [ZAM added and configured]()
- [ ] [Optional VM Installation with Nvida dGPU passthrough + post-install script (cant fully automate config during install, you need to create a VM first)]()
- [ ] [Optional Docker Installation]()
- [ ] [Secured system and kernal]()
- [ ] [Clock set by Chrony NTP]()
- [ ] [Laptop auto detect to install power saving options and TLP]()
- [ ] [HyprOS Settings and scripts App (prime-run, xrun configs) TBA]()
- [ ] [HyprWorld config App (themes, wallpaper configs) TBA]()

### Todo
- [ Waiting ] [Intergrate archinstall partition manager](https://)
- [ started ] [Intergrate Laptop sepcific scripts](https://)
- [ Waiting ] [Intel gpu](https://wiki.archlinux.org/title/intel_graphics)
- [ Waiting ] [GVT-g for old intel](https://wiki.archlinux.org/title/Intel_GVT-g)
- [ dep updates ] [GVT-g for modern intel](https://wiki.archlinux.org/title/Intel_GVT-g)
- plymouth
- vm passthrough

### Customise script
Once you download the script you can modify the script before or during installation via the install script. 
it modifies the files in the directory to your adjested preferences. Or you can change the files in the data directory manually. 

### Installation Instructions
```
git clone
cd hypros
sudo ./install.sh
```
### Snapshot recovery instructions
ENTER BIOS: use admin password to boot into efi-shell image
Inspect recovery script using edit FS0:\recovery.nsh (if FS0 is not your hard disk, try other FSn)
Run the script using FS0:\recovery.nsh
Once recovered, remove efi-shell entry from UEFI

### Restic server installation steps


### Thank you to</H2>
[hyprdots](https://github.com/prasanthrangan)
[JaKooLit](https://github.com/JaKooLit/)
[ML4W](https://gitlab.com/stephan-raabe/dotfiles)
[Gl00ria](https://github.com/Gl00ria/dotfiles/tree/main/dot_hyprland)
[TommtTran732 - Arch install script](https://github.com/TommyTran732/Arch-Setup-Script)
[Arch Wiki and User posts](https://archlinux.org)
