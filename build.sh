#!/bin/bash

#!/bin/bash

# Function to check if Transmission is installed
check_transmission() {
    if ! command -v transmission-cli &> /dev/null; then
        echo "Transmission-cli is not installed. Installing..."
        sudo pacman -Sy transmission-cli --noconfirm
    fi
}

# Function to download Arch Linux ISO using torrent
download_arch_iso() {
    check_transmission
    echo "Downloading Arch Linux ISO via torrent..."
    transmission-cli -w ~/Downloads/ 'magnet:?xt=urn:btih:a9cd8c3b9dcad4785db47908ad63d43694432cae&dn=archlinux-2024.02.01-x86_64.iso'
}

# Function to create bootable USB drive with Arch Linux ISO
create_bootable_usb() {
    echo "Creating bootable USB drive..."
    sudo dd if=~/Downloads/archlinux-*.iso of=/dev/sdX bs=4M status=progress
    sync
}

# Function to clone GitHub repository and copy folder
clone_and_copy() {
    echo "Cloning GitHub repository..."
    git clone https://github.com/name/project.git
    echo "Copying folder onto Arch Linux ISO..."
    cp -r project ~/mnt/  # Replace ~/mnt/ with the path where your installation media is mounted
}

# Function to configure automatic execution of install.sh
configure_autorun() {
    echo "Configuring automatic execution of install.sh..."
    echo "cd /mnt/project && ./install.sh" >> ~/mnt/archiso/airootfs/root/customize_airootfs.sh
}

# Main function
main() {
    download_arch_iso
    create_bootable_usb
    clone_and_copy
    configure_autorun
}

main

#########################################################################
# Function to download torrent file
download_torrent() {
    local torrent_link="$1"
    local save_path="${2:-/Downloads}"

    # Check if transmission-cli is installed
    if ! command -v transmission-cli &> /dev/null; then
        echo "Error: transmission-cli is not installed. Please install it and try again."
        exit 1
    fi

    # Download torrent file
    transmission-cli "$torrent_link" -w "$save_path"
}

# Function to download file from mirror
download_from_mirror() {
    local mirror_link="$1"
    local save_path="${2:-/Downloads}"

    # Check if wget is installed
    if ! command -v wget &> /dev/null; then
        echo "Error: wget is not installed. Please install it and try again."
        exit 1
    fi

    # Download file from mirror
    wget "$mirror_link" -P "$save_path"
}

# Function to verify checksum
verify_checksum() {
    local file_path="$1"
    local expected_checksum="$2"

    # Check if sha256sum is installed
    if ! command -v sha256sum &> /dev/null; then
        echo "Error: sha256sum is not installed. Please install it and try again."
        exit 1
    fi

    # Calculate actual checksum
    local actual_checksum=$(sha256sum "$file_path" | awk '{print $1}')

    # Compare checksums
    [[ "$actual_checksum" == "$expected_checksum" ]]
}

# Function to cleanup temporary files
cleanup() {
    rm -f "$temp_file"
}

# Trap to ensure cleanup upon script exit
trap cleanup EXIT

# Function to extract country names and mirror links
extract_countries_and_mirrors() {
    local temp_file="$1"
    local in_country=false
    local in_mirrors=false

    while IFS= read -r line; do
        if [[ $line =~ "<h5><span class=\"fam-flag fam-flag-" ]]; then
            in_country=true
            in_mirrors=false
            country=$(echo "$line" | sed -E 's/^.*title="([^"]+)".*$/\1/')
            echo "$country"
        elif [[ $line =~ "<ul>" ]]; then
            in_mirrors=true
        elif [[ $line =~ "</ul>" ]]; then
            in_mirrors=false
        elif [[ $in_mirrors == true && $line =~ "<li><a href=\"([^\"]+)\" title=\"Download from ([^\"]+)\">" ]]; then
            mirror="${BASH_REMATCH[1]} - ${BASH_REMATCH[2]}"
            echo "$mirror"
        fi
    done < "$temp_file"
}

# Function to display options to the user
display_options() {
    local options=("$@")
    local index=1

    for option in "${options[@]}"; do
        echo "$index. $option"
        ((index++))
    done
}

# Fetch webpage content and store it in a temporary file
temp_file=$(mktemp)
url="https://archlinux.org/download/"
curl -s "$url" > "$temp_file"

# Extract torrent link
torrent_link=$(grep -oE 'magnet:[^"]+' "$temp_file")

# Extract country names and mirror links
countries_and_mirrors=($(extract_countries_and_mirrors "$temp_file"))

# Display download options
echo "Choose download option:"
display_options "Torrent" "HTTP Mirror"
read -rp "Enter option number: " option

if [[ "$option" == "1" ]]; then
    read -rp "Enter save path for torrent file (default: /Downloads): " save_path
    download_torrent "$torrent_link" "$save_path"
    echo "Torrent download complete!"
elif [[ "$option" == "2" ]]; then
    echo "HTTP Mirror option selected."
    echo "Choose country:"
    display_options "${countries_and_mirrors[@]}"
    read -rp "Enter country option number: " country_option
    selected_country="${countries_and_mirrors[$((country_option - 1))]}"

    echo "Mirrors in $selected_country:"
    mirrors=($(grep -A 1 -m 1 "^$selected_country$" "$temp_file" | grep -oP '<a href="\K[^"]+'))
    display_options "${mirrors[@]}"
    read -rp "Enter mirror option number: " mirror_option
    selected_mirror="${mirrors[$((mirror_option - 1))]}"

    echo "Selected mirror: $selected_mirror"
    # Implement mirror download logic here
    read -rp "Enter save path for ISO file (default: /Downloads): " save_path
    download_from_mirror "$selected_mirror" "$save_path"
    echo "Download complete!"

    # Checksum verification
    checksum_link="${selected_mirror%/}/SHA256SUMS"
    expected_checksum=$(curl -s "$checksum_link" | head -n 1 | awk '{print $1}')
    
    if verify_checksum "$file_path" "$expected_checksum"; then
        echo "Checksum verification passed!"
    else
        echo "Checksum verification failed!"
    fi
else
    echo "Invalid option. Exiting."
    exit 1
fi
