echo " Installing Packages for ASUS ROG..."
for ASUS in asusctl supergfxctl rog-control-center; do
install_package  "$ASUS" 2>&1 | tee -a "$LOG"
  if [ $? -ne 0 ]; then
  echo -e "\e[1A\e[K${ERROR} - $ASUS install failed."
  exit 1
  fi
done

echo " Activating ROG services..."
sudo systemctl enable --now supergfxd 2>&1 | tee -a "$LOG"