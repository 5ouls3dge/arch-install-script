#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| HyprOS - Install Script             |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#
source scripts/global_fn.sh
source scripts/global_log.sh
source scripts/global_header.sh

# Check if root. If root, script will exit.
if [[ $EUID -eq 0 ]]; then
    echo "Do not run this script as root! Exiting..."
    exit 1
fi

PS3="Select an option: "

# Installation Options menu
installation_options() {
    clear
    display_logo
    echo
    PS3="Select an installation option: "
    select install_option in "Install HyprVerse" "Install HyprWorld" "Package Manager" "Docker Manager" "Back to Main Menu"
    do
        case $install_option in
            "Install Desktop/ Laptop")
                echo "Installing HyprVerse..."
                # Add your installation commands here
                ;;
            "Install Server")
                echo "Installing HyprWorld..."
                # Add your installation commands here
                ;;
            "Back to Main Menu")
                return
                ;;
            *)
                echo "Invalid option. Please select a valid option."
                ;;
        esac
    done
}

# Run Scripts menu
run_scripts() {
    clear
    display_logo
    echo
    PS3="Select a script to run: "
    select script_option in "Run Nvidia Setup" "Run Plymouth Install" "Back to Main Menu"
    do
        case $script_option in
            "Run Nvidia Setup")
                echo "Running Nvidia Setup..."
                # Add your script execution commands here
                ;;
            "Run Plymouth Install")
                echo "Running Plymouth Install..."
                # Add your script execution commands here
                ;;
            "Back to Main Menu")
                return
                ;;
            *)
                echo "Invalid option. Please select a valid option."
                ;;
        esac
    done
}

# Customise HyprOS menu
customise_hypros() {
    display_logo
    PS3="Select a script edit: "
    select script_option in "Edit OS Packages" "Disable parts of the install" "Back to Main Menu"
    do
        case $script_option in
            "Edit OS Packages")
                /scripts/install_apps.sh
                ;;
            "Disable features of the install (Uncomment parts of the install to DISABLE...)")
                echo ""
                /scripts/disable_features.sh

                ;;
            "Back to Main Menu")
                return
                ;;
            *)
                echo "Invalid option. Please select a valid option."
                ;;
        esac
    done
}

while true
do
    display_logo
    echo
    echo "Welcome to ScriptOS, enjoy the Bash!"
    echo
    echo "Install ScriptOS: "
    echo "1) ScriptOS Desktop - ArchLinux Desktop and Laptop installation"
    echo "2) ScriptOS Server - Debian Server install (stable or SID)"
    echo
    echo "Tools"
    echo "3) ScriptOS Package Manager - Install apps and configure prime_run and xrun"
    echo "4) Docker Manager - Simple docker container manager"

    read -rp "Select an option: " main_option

    case $main_option in
        1)
            installation_options
            ;;
        2)
            run_scripts
            ;;
        3)
            customise_hypros
            ;;
        4)
            clear
            exit
            ;;
        *)
            echo "Invalid option. Please select a valid option."
            ;;
    esac
    # Clear screen after returning from a submenu
    [[ "$main_option" =~ [1-3] ]] && clear
done
