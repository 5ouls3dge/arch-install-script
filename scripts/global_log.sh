#!/bin/bash

# Define log file path
LOG_FILE="/var/log/install.log"

# Ensure the log file exists and is writable
touch "$LOG_FILE" && chmod 666 "$LOG_FILE"

# Function to log messages to the log file
log_message() {
    local level=$1
    local message=$2
    local timestamp=$(date +"%Y-%m-%d %H:%M:%S")
    echo "[$timestamp] [$level] $message" >> "$LOG_FILE"
}

## Example usage:
#log_message "INFO" "Installation started"
## Run installation commands and log their output
#apt-get update >> "$LOG_FILE" 2>&1
#apt-get install -y some-package >> "$LOG_FILE" 2>&1 || log_message "ERROR" "Failed to install some-package"
#log_message "INFO" "Installation completed"