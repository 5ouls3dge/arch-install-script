#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| User enters custom commands         |--/ /-|#
#|-/ /--| # by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

# Read multi-line input from user
read_multiline_input() {
    echo "Enter your commands. Press Ctrl+D when you're done."
    cat > cust_comm_temp.sh
}

# Execute the commands from the file
execute_commands() {
    echo "Running the commands..."
    chmod +x cust_comm_temp.sh
    ./cust_comm_temp.sh
}

# Main menu
while true; do
    global_logo
    echo
    echo "Select an option:"
    echo "1. Enter custom commands"
    echo "2. Run commands"
    echo "3. Cancel"
    echo
    read -p "Option: " option

    case $option in
        1)
            read_multiline_input
            ;;
        2)
            execute_commands
            exit
            ;;
        3)
            echo "Operation canceled."
            exit
            ;;
        *)
            echo "Invalid option. Please enter a valid option."
            ;;
    esac
done
