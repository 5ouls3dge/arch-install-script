#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| Install and configure Docker        |--/ /-|#
#|-/ /--| # by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#
echo "Installing Docker..."

# Update package database and install required dependencies
pacman -Sy --noconfirm docker docker-compose

# Set Docker to use ipV6 
read -p "Set Docker to use IPV6 (y/n)? " docker_ipv6

if [ "$docker_ipv6" == "y" ]; then
    mkdir -p /etc/docker
cat << EOF > /etc/docker/daemon.json
{
  "ipv6": true,
  "fixed-cidr-v6": "fd00::/80",
  "storage-driver": "btrfs"
}
EOF
    echo "Docker Now configured to use IPV6"
else
    echo "Docker will use IPV4"
fi
