#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| HyprOS - Instal HyprWorld           |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

git clone https://gitlab.com/5ouls3dge/hyprworld
cd hyprworld
chmod +x install.sh
sudo ./install.sh