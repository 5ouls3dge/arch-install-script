#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| Restic setup for local backup       |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

# Function to check if a package is installed and install it if necessary
echo "Installing Restic on the local machine..."

install_package() {
    if ! pacman -Q "$1" &>/dev/null; then
        echo "Installing $1..."
        sudo pacman -S --noconfirm "$1"
    fi
}

# Install packages
install_package "btrfs-progs"
install_package "restic"

# Prompt for the path to create the Restic repository
read -p "Enter the path to create the Restic repository to store your backups (e.g., /srv/restic-repo): " repo_path

# Initialize the Restic repository
restic init --repo "$repo_path"

# Create a Systemd service file
SERVICE_FILE="/etc/systemd/system/restic-backup.service"
echo "[Unit]
Description=Restic Backup Service

[Service]
Type=simple
ExecStart=/usr/bin/restic backup "\$BACKUPDESTINATION" --repo "????"
[Install]
WantedBy=default.target" | sudo tee "$SERVICE_FILE" > /dev/null

# Create a Systemd timer file
TIMER_FILE="/etc/systemd/system/restic-backup.timer"
echo "[Unit]
Description=Run Restic Backup regularly

[Timer]
OnCalendar=daily
Persistent=true

[Install]
WantedBy=timers.target" | sudo tee "$TIMER_FILE" > /dev/null

# Enable and start the timer
sudo systemctl enable --now restic-backup.timer

echo "Restic installation and setup completed successfully on local and remote machines."
fi
