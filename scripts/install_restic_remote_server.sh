#!/bin/bash
###################################################
#                                                 #
#           HIGHLY BUGGY AND EXPERIMENTAL         #
#                                                 #
###################################################

#######################
# NOTES:
# 1: Multiple scenirios a) nginx b) traefik c) regular d) local server connect (LAN) only
# 2: signed certs or selfsigned
# 3: require WAN IP or domain for a, b, c?
# 4: c and d are important. Anyone running a or b is adavance user? Maybe still run c script, and let them configure a and b. provide example:

######### Regular setup.
# Todo 
# Sign cert and fix TLS

# Prompt user for values
read -p "Enter the remote username: " set_remote_user
read -p "Enter the remote server address: " set_remote_address
read -p "Enter the remote REST server port (default is 8000): " set_remote_rest_server_port
read -p "Enter the remote REST server repository path, press enter for the script to locate: " set_remote_restic_repo_path

# Default values if user input is empty
REMOTE_USER=${set_remote_user}
REMOTE_SERVER_ADDRESS=${set_remote_address}
REMOTE_REST_SERVER_PORT=${set_remote_rest_server_port}
REMOTE_RESTIC_REPO_PATH="${set_remote_restic_repo_path}"
REMOTE_HOME_DIR="~"

# Using htpassword located in restic repo plus TLS signed cert.
# To prevent your users from accessing each others' repositories, you may use the --private-repos flag which grants access 
# only when a subdirectory with the same name as the user is specified in the repository URL.

# Create htpasswd
htpasswd -B -c /tmp/temp_htpasswd "${REMOTE_USER}"

# Create directory exists on the server
ssh "${REMOTE_USER}@${REMOTE_SERVER_ADDRESS}" "mkdir -p ${REMOTE_RESTIC_REPO_PATH}/${REMOTE_USER}" && wait

# Copy the temporary .htpasswd file to the server
scp /tmp/temp_htpasswd "${REMOTE_USER}@${REMOTE_SERVER_ADDRESS}:${REMOTE_RESTIC_REPO_PATH}/.htpasswd"  && wait

# Remove the temporary .htpasswd file
rm /tmp/temp_htpasswd

# Generate host ssh key
ssh-keygen -t rsa -b 2048 -f ~/.ssh/id_rsa -N ""

# Create temporary directory to store the script
SCRIPT_DIR=$(mktemp -d)
SCRIPT_FILE="$SCRIPT_DIR/server_setup_restic.sh"

# First script content
cat <<EOL > "$SCRIPT_FILE"
#!/bin/bash
REMOTE_USER=${REMOTE_USER}
REMOTE_SERVER_ADDRESS=${REMOTE_SERVER_ADDRESS}
REMOTE_REST_SERVER_PORT=${REMOTE_REST_SERVER_PORT}
REMOTE_RESTIC_REPO_PATH=${REMOTE_RESTIC_REPO_PATH}
REMOTE_HOME_DIR=${REMOTE_HOME_DIR}

# change ownership of the backup directory
sudo chown -R ${REMOTE_USER}:${REMOTE_USER} ${REMOTE_RESTIC_REPO_PATH}/${REMOTE_USER}

# Detect Linux distribution and install Restic
if command -v apt-get &> /dev/null; then
    # Debian/Ubuntu
    sudo apt install bunzip2
    wget https://github.com/restic/restic/releases/download/v0.9.5/restic_0.9.5_linux_amd64.bz2
    bunzip2 restic*.bz2
    sudo cp restic*amd64 /usr/local/bin/restic
    # To use the REST server (which provides faster transfer speeds than SSH), it will need to be downloaded and compiled as follows:
    sudo apt install golang git
    sudo apt install apache2-utils # for htpasswd
    cd ~/
    git clone https://github.com/restic/rest-server.git
    cd rest-server
    sudo make install
elif command -v dnf &> /dev/null; then
    # Fedora
    sudo dnf copr enable copart/restic
    sudo dnf install restic
    sudo dnf install golang
    sudo dnf install apache2-utils # for htpasswd
elif command -v zypper &> /dev/null; then
    # openSUSE
    sudo zypper install -y restic rest-server
elif command -v pacman &> /dev/null; then
    # Arch Linux
    # Check if yay is installed
    if ! command -v yay &> /dev/null; then
        echo "yay not found. Installing yay..."
        sudo pacman -S --noconfirm yay
    fi
    # Install Restic rest-server pache2-utils
    yay -S --noconfirm restic restic-rest-server apache2-utils
elif command -v apk &> /dev/null; then
    # Arm Based 
    sudo apk add restic rest-server
    wget https://github.com/restic/rest-server/releases/download/v0.11.0/rest-server_0.11.0_linux_arm64.tar.gz
tar xzf rest-server_0.11.0_linux_arm64.tar.gz
cp rest-server_0.11.0_linux_arm64/rest-server /usr/local/bin/restic-server
else
    echo "Unsupported Linux distribution. Please enter Restic rest-server apache2-utils install command for your server distro manually."
    echo
    read -p "Enter the restic and rest-server package install command for your distribution (press 'x' to skip): " RESTIC_INSTALL_CMD
    if [ "\$RESTIC_INSTALL_CMD" != "x" ]; then
        eval "\$RESTIC_INSTALL_CMD"
    else
        echo "Restic installation skipped by user."
    fi
fi

# Verify Restic installation
if command -v restic &> /dev/null; then
    echo "Restic has been installed successfully."
else
    echo "Failed to install Restic. Please check your package manager or install it manually."
fi

# Verify rest-server installation
if command -v rest-server &> /dev/null; then
    echo "Failed to install restic. Please check your package manager or install it manually."
else
    echo "Failed to install rest-server. Please check your package manager or install it manually."
fi

# Setup TLS
# Generate self-signed TLS keys
openssl req -newkey rsa:2048 -nodes -x509 -keyout /etc/rest-server/private_key -out /etc/rest-server/public_key -days 365 -addext "subjectAltName = IP:127.0.0.1,DNS:$REMOTE_SERVER_ADDRESS"

# Set permissions for TLS keys
chmod 600 /etc/rest-server/private_key
chmod 644 /etc/rest-server/public_key

# Start Rest Server with TLS support
rest-server --path $REMOTE_RESTIC_REPO_PATH --tls --tls-cert /etc/rest-server/public_key --tls-key /etc/rest-server/private_key --append-only --private-repos --no-auth --listen :$REMOTE_REST_SERVER_PORT

# Start rest-server
echo "Starting rest-server on port \$REMOTE_REST_SERVER_PORT..."
rest-server --path \$REMOTE_RESTIC_REPO_PATH --no-auth --listen :\$REMOTE_REST_SERVER_PORT &
REST_SERVER_PID=\$!

# Ensure the server is running
sleep 2

# Display server information
echo "Restic server is running on port \$REMOTE_REST_SERVER_PORT"
echo "Repository path: \$REMOTE_RESTIC_REPO_PATH"

# Wait for the user to press Enter before continuing
read -p "Press Enter to initialize restic repository and configure the server..."

# Initialize restic repository
echo "Initializing restic repository..."
restic -r "rest:http://localhost:\$REMOTE_REST_SERVER_PORT\$REMOTE_RESTIC_REPO_PATH" init

# Replace the values in the config file
new_listen="localhost:\$REMOTE_REST_SERVER_PORT"
new_no_auth="false"
sed -i "s/listen = .*/listen = \$new_listen/" \$REMOTE_RESTIC_REPO_PATH/config
sed -i "s/no-auth = .*/no-auth = \$new_no_auth/" \$REMOTE_RESTIC_REPO_PATH/config


echo "Configuration completed."
EOL

# Copy script to server
read -p "Copy server_setup_restic.sh script to $REMOTE_USER@$REMOTE_SERVER_ADDRESS:$REMOTE_HOME_DIR? (y/n): " COPY_RESPONSE

if [[ "$COPY_RESPONSE" =~ ^[Yy]$ ]]; then
    # Copy script to remote server
    scp -r "$SCRIPT_DIR" "$REMOTE_USER@$REMOTE_SERVER_ADDRESS:$REMOTE_HOME_DIR"

    # Prompt user to execute the script on the server
    read -p "Execute server_setup_restic.sh on $REMOTE_USER@$REMOTE_SERVER_ADDRESS? (y/n): " EXECUTE_RESPONSE

    if [[ "$EXECUTE_RESPONSE" =~ ^[Yy]$ ]]; then
        # Execute script on remote server
        ssh "$REMOTE_USER@$REMOTE_SERVER_ADDRESS" "bash $REMOTE_HOME_DIR/server_setup_restic.sh"
    else
        echo "Script not executed. You can manually execute it on the server."
    fi
else
    echo "Script not copied. You can manually copy and execute it on the server."
fi

# Remove temporary directory
rm -rf "$SCRIPT_DIR"

# Install Restic on the local machine
echo "Installing Restic on the local machine..."
if command -v restic &> /dev/null; then
    echo "Restic is already installed."
else
        sudo yay -S restic
    fi

    # Initialize restic repository on the local machine
    echo "Initializing restic repository on the local machine..."
    restic init --repo "rest:http://\$REMOTE_SERVER_ADDRESS:\$REMOTE_REST_SERVER_PORT/\$REMOTE_RESTIC_REPO_PATH"

    # Create a Systemd service file
    SERVICE_FILE="/etc/systemd/system/restic-backup.service"
    echo "[Unit]
    Description=Restic Backup Service

    [Service]
    Type=simple
    ExecStart=/usr/bin/restic backup "\$BACKUPDESTINATION" --repo "rest:http://\$REMOTE_SERVER_ADDRESS:\$REMOTE_REST_SERVER_PORT/\$REMOTE_RESTIC_REPO_PATH"
    [Install]
    WantedBy=default.target" | sudo tee "$SERVICE_FILE" > /dev/null

    # Create a Systemd timer file
    TIMER_FILE="/etc/systemd/system/restic-backup.timer"
    echo "[Unit]
    Description=Run Restic Backup regularly

    [Timer]
    OnCalendar=daily
    Persistent=true

    [Install]
    WantedBy=timers.target" | sudo tee "$TIMER_FILE" > /dev/null

    # Enable and start the timer
    sudo systemctl enable --now restic-backup.timer

    echo "Restic installation and setup completed successfully on local and remote machines."
fi



# Remove temporary file
rm -f "$TEMP_FILE"

# Variables
# Prompt user for login
while true; do
    read -p "Do you want to log in to your server and run the install script? (y/n): " LOGIN_RESPONSE
    if [[ "$LOGIN_RESPONSE" =~ ^[YyNn]$ ]]; then
        break
    else
        echo "Invalid input. Please enter 'y' or 'n'."
    fi
done

if [[ "$LOGIN_RESPONSE" =~ ^[Yy]$ ]]; then
    # SSH into the server in the background
    ssh "$REMOTE_USER@$REMOTE_SERVER_ADDRESS" &

    # Store the PID (Process ID) of the background process
    BG_PID=$!

    # Check if the script exists on the server
    SCRIPT_PATH="$REMOTE_HOME_DIR/server_setup_restic.sh"
    if ssh "$REMOTE_USER@$REMOTE_SERVER_ADDRESS" "[ -e $SCRIPT_PATH ]"; then
        # Run the install script on the server
        ssh "$REMOTE_USER@$REMOTE_SERVER_ADDRESS" "bash $SCRIPT_PATH"
        echo "You have returned to the local machine."
    else
        echo "Please login to the server and run the script located at $SCRIPT_PATH."
    fi

    # Wait for the background process to finish
    wait $BG_PID
else
    echo "You can manually log in and run the script later."
fi