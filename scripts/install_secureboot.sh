#!/bin/bash
#|---/ /+--------------------------+---/ /|#
#|--/ /-| Secureboot setup         |--/ /-|#
#|-/ /--| # by 5ouls3edge (2024)   |-/ /--|#
#|/ /---+--------------------------+/ /---|#
clear


# Echo BIOS setup instructions
global_logo
echo
echo "Before proceeding with the secureboot installation you MUST have ALREADY completed the following tasks:"
echo
echo "BIOS Setup Instructions:"
echo "1. Set admin password."
echo "2. Disable Secure Boot."
echo "3. Delete all Secure Boot keys."
echo "4. Save and exit BIOS. "
echo "5. Now you can start the ScriptOS installation."
echo
# Prompt user to confirm BIOS setup completion
while true; do
    read -p "Have you completed the BIOS setup as instructed? Type YES in CAPITALS to continue or x to skip: " set_confirmation

    if [ "$setup_confirmation" != "YES" ] && [ "$setup_confirmation" != "x" ]; then
        continue
    fi

    break
done

# Install arch-secure-boot
if [ "$set_confirmation" == "YES" ]; then
    # Proceed with the installation and script execution
    echo "Starting secureboot installation..."

    yay -S --noconfirm arch-secure-boot

    # Setup arch-secure-boot (command runs all the steps in the proper order)
    arch-secure-boot initial-setup 

    # Add Nvidia instructions to kernal
    # Define the line to be added
    LINE="nvidia-drm.modeset=1"

    # Check if the file exists
    if [ -f "/etc/kernel/cmdline" ]; then
        # File exists, append the line to it
        echo "$LINE" | sudo tee -a /etc/kernel/cmdline > /dev/null
        echo "Line added to /etc/kernel/cmdline"
    else
        # File doesn't exist, create it and add the line
        echo "$LINE" | sudo tee /etc/kernel/cmdline > /dev/null
        echo "File created with line added: /etc/kernel/cmdline"
    fi
fi

# Skip arch-secure-boot options
if [ "$set_confirmation" != "YES" ]; then
    echo
    echo "BIOS setup not confirmed... Please select from the following options: "
    echo
    echo "1) Restart into UFEI Settings to perform the above required actions"
    echo "2) Skip arch-secure-boot setup. Continuing ScriptOS installation"
    echo "3) Restart machine"
    echo "4) Shutdown machine"
    echo
    read -r -p "Please enter option 1, 2, 3 or 4: " set_failed_option
fi

# Validate user input
while true; do
    read -r -p "Option not Valid. Please enter a valid option, either 1, 2, 3, 4 : " set_failed_option
    case $set_failed_option in
        1|2|3|4)
            break ;;
        *)
            continue ;;
    esac
done

# Restart into UEFI settings
if [ "$set_failed_option" == "1" ]; then
    echo "Restarting into UEFI Settings"
    systemctl reboot --firmware-setup
fi

# Continue Install and skip arch-secure-boot setup
if [ "$set_failed_option" == "2" ]; then
    echo "Skipping arch-secure-boot install. Continuing ScriptOS installation"
    # No action needed
fi

# Restart System
if [ "$set_failed_option" == "3" ]; then
    echo "Restarting now..."
    systemctl reboot
fi

# Shutdown system
if [ "$set_failed_option" == "4" ]; then
    echo "Shutting down now"
    systemctl poweroff
fi
