#!/bin/bash
#|---/ /+--------------------------+---/ /|#
#|--/ /-| ZRAM Installation Script |--/ /-|#
#|-/ /--| # by 5ouls3edge (2024)   |-/ /--|#
#|/ /---+--------------------------+/ /---|#

sudo modprobe zram num_devices=1

# Set compression algorithm (example: zstd) ...
sudo bash -c "echo zstd > /sys/block/zram0/comp_algorithm"

# Activate zram0 device with the highest priority setting of 32767 ...
sudo mkswap --label zram0 /dev/zram0
sudo swapon --priority 32767 /dev/zram0

# ZRAM Kernel parameters
cat <<EOF | sudo tee -a /etc/sysctl.d/99-sysctl.conf
# ZRAM Configuration
# Increase frequency of clearing caches to free RAM
vm.vfs_cache_pressure=500

# Increase percentage to start using zram earlier
vm.swappiness=100

# Amount of memory pages permitted to be "dirty" before writing to zram
vm.dirty_background_ratio=1
vm.dirty_ratio=50
EOF

# Better IO Scheduler
cat << EOF > /etc/udev/rules.d/60-ioschedulers.rules
# set scheduler for NVMe
ACTION=="add|change", KERNEL=="nvme[0-9]*", ATTR{queue/scheduler}="none"
# set scheduler for SSD and eMMC
ACTION=="add|change", KERNEL=="sd[a-z]|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"
# set scheduler for rotating disks
ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
EOF

# ZRAM configuration
bash -c 'cat > /mnt/etc/systemd/zram-generator.conf' <<-'EOF'
[zram0]
zram-fraction = 1
max-zram-size = 8192
EOF