#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| HyprOS - Linux Installation Script  |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

# User config prompts
./setup_user_prompts.sh

# Synchronize package databases
pacman -Syy

# Install necessary packages
pacman -S --noconfirm btrfs-progs cryptsetup snapper

# Partition setup
./setup_partitions.sh

# Install Arch Linux system packaes
pacstrap $MOUNT_POINT base base-devel \
    ${kernel} ${microcode} linux-firmware \
    snapper snap-pac efibootmgr \
    sudo networkmanager apparmor firewalld \
    zram-generator reflector chrony sbctl \
    openssh fwupd profile-sync-daemon haveged irqbalance

# Setup arch system
./setup_system.sh

# Install Security and Performance Tweaks ##
./setup_linux_tweaks.sh

# Chroot into the new system and install and configure software packages
./setup_chroot.sh

# Post system service setup
./setup_systemctl.sh

# Setting umask to 077.
sed -i 's/022/077/g' /mnt/etc/profile
echo "" >> /mnt/etc/bash.bashrc
echo "umask 077" >> /mnt/etc/bash.bashrc

# Finishing up
./setup_finish.sh
