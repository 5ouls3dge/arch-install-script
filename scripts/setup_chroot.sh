#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| HyprOS - chroot setup               |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

arch-chroot $MOUNT_POINT /bin/bash <<EOF
    ## SYSTEM ##
    # Update the package databases
    pacman -Syy

    # Upgrade all installed packages
    pacman -Syu

    # Install Applications #
    pacman -Sy appamour 

    # Package Manager - Choose and install additional packages #
    ./setup_package_manager.sh

    # System config
    ./setup_chroot_system.sh

    ## VM installation ##
    if [ "$VMINSTALL" == "y" ]; then
        ./install_vm.sh
    else
        echo "skipping VM installation"
    fi

    ## Docker installation ##
    if [ "$DOCKERINSTALL" == "y" ]; then
        ./install_docker.sh
    else
        echo "skipping Docker installation"
    fi

    ## Install HyprWorld (riced hyprland) ##
    if [ "$GPU" == "NVIDIA" ]; then
        ./install_hyprworld_nvidia.sh
    else
        ./install_hyprworld.sh
    fi

    ## Configure prime-run apps ##
    if [ "$GPU" == "NVIDIA" ]; then
        ./setup_nvidia_prime.sh
    else
        echo "Skipping NVIDIA Prime setup..."
    fi

    ## Configure xrun apps ##
    if [ "$GPU" == "NVIDIA" ]; then
        ./setup_nvidia_xrun.sh
    else
        echo "Skipping NVIDIA xrun setup..."
    fi
    
    ## Hide prime-run and xrun original app ##
    if [ "$GPU" == "NVIDIA" ]; then
        ./setup_nvidia_hide_original_app.sh
    else
        echo "Skipping hidding original app from /usr/applications/"
    fi

    ## Install Restic
    if [ "$RESTICINSTALL" == "r" ]; then
        ./install_restic_remote.sh
    elif [ "$RESTICINSTALL" == "l" ]; then
        ./install_restic_local.sh
    else
        echo "Skipping Restic installation"
    fi

    ## Set snapper time limits to keep snapsots ##
    ./setup_snapper.sh

    ## Preventing snapshot slowdowns ##
    echo 'PRUNENAMES = "snapshots"' >> /etc/updatedb.conf

    ## Laptop power saving configurations ##
    if [ "$ISLAPTOP" == "y" ]; then
        ./setup_laptop.sh
    else
        echo "Machine is a Desktop. Skipping laptop power saving options..."
    fi

    ## Network Manager iwd backend ##
    cat << EOF >> /etc/NetworkManager/conf.d/nm.conf
    [device]
    wifi.backend=iwd
    EOF

    ## Set up reflector ##
    cat << EOF > /etc/xdg/reflector/reflector.conf
    # Set the output path where the mirrorlist will be saved (--save).
    --save /etc/pacman.d/mirrorlist
    # Select the transfer protocol (--protocol).
    --protocol https
    # Use only the most recently synchronized mirrors (--latest).
    --latest 100
    # Sort the mirrors by MirrorStatus score
    --sort score
    EOF

    ## ZRAM ##
    ./install_zram.sh

    ## Nftables ##
    ./setup_nftables.sh $DOCKERINSTALL

    ## Prompt user to enter custom commands
    ./install_custom_commands.sh

    ## System config ##
    sed -i 's/^umask.*/umask\ 077/' /etc/profile && \
    chmod 700 /etc/{iptables,arptables,nftables.conf} && \
    echo "auth optional pam_faildelay.so delay=4000000" >> /etc/pam.d/system-login && \
    echo "tcp_bbr" > /etc/modules-load.d/bbr.conf && \
    echo "write-cache" > /etc/apparmor/parser.conf

    ## Generating a new initramfs ##
    echo "Creating a new initramfs."
    chmod 600 /boot/initramfs-linux* &>/dev/null
    mkinitcpio -P &>/dev/null
    
    ## Secureboot (no bootloader) install ##
    if [ "$BOOTLOADER" == "secureboot" ]; then
        ./install_secureboot
    fi
EOF
