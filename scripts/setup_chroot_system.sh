#!/bin/bash
#|---/ /+--------------------------+---/ /|#
#|--/ /-| Chroot system config     |--/ /-|#
#|-/ /--| # by 5ouls3edge (2024)   |-/ /--|#
#|/ /---+--------------------------+/ /---|#
    
# Enabling NTS
cat <<EOF > /etc/chrony.conf
# Use public NTP servers from the pool.ntp.org project.
server 0.pool.ntp.org offline
server 1.pool.ntp.org offline
server 2.pool.ntp.org offline
server 3.pool.ntp.org offline

# Record the rate at which the system clock gains/losses time.
driftfile /etc/chrony.drift

# In first three updates step the system clock instead of slew
# if the adjustment is larger than 1 second.
makestep 1.0 3

# Enable kernel synchronization of the real-time clock (RTC).
rtcsync

rtconutc
EOF

# Set the timezone
ln -sf /usr/share/zoneinfo/$(curl -s http://ip-api.com/line?fields=timezone) /etc/localtime &>/dev/null
hwclock --systohc

# Generating locales
echo "Generating locales."
locale-gen &>/dev/null

# Set the root password
passwd

## USER ACCOUNT SETUP ##
# Adding user with sudo privilege
if [ -n "$USERNAME" ]; then
    echo "Adding $USERNAME with root privilege."

    case "$VMINSTALL$DOCKERINSTALL" in
        "yy") groups="docker,input,kvm,libvirt,storage,video,wheel" ;;
        "yn") groups="input,kvm,libvirt,storage,video,wheel" ;;
        "ny") groups="docker,input,storage,video,wheel" ;;
        "nn") groups="input,storage,video,wheel" ;;
    esac

    useradd -m -G "$groups" -s /bin/zsh "$USERNAME"
    usermod -aG wheel "$USERNAME"
    groupadd -r audit
    gpasswd -a "$USERNAME" audit
fi

## Setting user password ##
[ -n "$USERNAME" ] && echo "Setting user password for ${USERNAME}." && echo -e "${PASSWORD}\n${PASSWORD}" | arch-chroot /mnt passwd "$USERNAME" >/dev/null

# Giving wheel user sudo access.
sed -i 's/# \(%wheel ALL=(ALL\(:ALL\|\)) ALL\)/\1/g' /mnt/etc/sudoers
echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" >> /mnt/etc/sudoers

# Prompt user for sudo timeout
read -p "Enter sudo timeout (in minutes, enter 0 for sudo password to be prompted everytime): " sudo_timeout
echo "Defaults timestamp_timeout=$sudo_timeout" >> /mnt/etc/sudoers

# Disable su for non-wheel users
bash -c 'cat > /mnt/etc/pam.d/su' <<-'EOF'
#%PAM-1.0
auth		sufficient	pam_rootok.so
# Uncomment the following line to implicitly trust users in the "wheel" group.
#auth		sufficient	pam_wheel.so trust use_uid
# Uncomment the following line to require a user to be in the "wheel" group.
auth		required	pam_wheel.so use_uid
auth		required	pam_unix.so
account		required	pam_unix.so
session		required	pam_unix.so
EOF