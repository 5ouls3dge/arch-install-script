#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| HyprOS - Exit cript                |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

display_logo() 

congratulations_message() {
    echo "Congratulations! Setup has finished successfully."
    echo "If you have installed secureboot or need to change your GPU setting, reboot to UEFI Settings."
    echo "Otherwise, you can reboot as normal or view install logs."
}

prompt_user() {
    echo "Select from the following choices:"
    echo "1. Reboot to UEFI Settings"
    echo "2. Reboot"
    echo "3. View log (nano ...)"
    read -p "Enter your choice (1, 2, or 3): " user_choice

    case $user_choice in
        1)
            echo "Rebooting to UEFI Settings..."
            systemctl reboot --firmware-setup
            ;;
        2)
            echo "Rebooting..."
            systemctl reboot
            ;;
        3)
            echo "Viewing install logs with nano..."
            nano /path/to/your/install-logs.log  # Replace with the actual path
            ;;
        *)
            echo "Invalid choice. Please enter 1, 2, or 3."
            ;;
    esac
}

main() {
    display_logo
    congratulations_message
    prompt_user
}

main
