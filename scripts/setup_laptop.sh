#!/bin/bash
#|---/ /+--------------------------+---/ /|#
#|--/ /-| Laptop Power Saving      |--/ /-|#
#|-/ /--| # by 5ouls3edge (2024)   |-/ /--|#
#|/ /---+--------------------------+/ /---|#


tweaks() {
# Audio Power Saving
if lspci -k | grep snd_ac97_codec | grep -q "Kernel driver in use: snd_hda_intel"; then
    echo "options snd_hda_intel power_save=1" > /etc/modprobe.d/audio_powersave.conf
else
    echo "options snd_ac97_codec power_save=1" > /etc/modprobe.d/audio_powersave.conf
fi

# PulseAudio
if [ -f /etc/pulse/default.pa ]; then
    sed -i 's/load-module module-suspend-on-idle/#load-module module-suspend-on-idle/' /etc/pulse/default.pa
    echo "File exists. Sed command executed successfully."
else
    echo "File does not exist. Skipping tweak..."
fi

# WiFi Power Saving for Intel wireless cards with iwlwifi driver
echo "options iwlwifi power_save=1" >> /etc/modprobe.d/iwlwifi.conf
echo "options iwlwifi uapsd_disable=0" >> /etc/modprobe.d/iwlwifi.conf

# Additional settings for iwlmvm
if lsmod | grep '^iwl.vm' &>/dev/null; then
    echo "options iwlmvm power_scheme=3" >> /etc/modprobe.d/iwlwifi.conf
else
    echo "options iwldvm force_cam=0" >> /etc/modprobe.d/iwlwifi.conf
fi

# SATA Active Link Power Management
echo 'ACTION=="add", SUBSYSTEM=="scsi_host", KERNEL=="host*", ATTR{link_power_management_policy}="med_power_with_dipm"' > /etc/udev/rules.d/hd_power_save.rules

# TLP Configuration
if [ "$GPU" == "AMD" ]; then
cat << EOF > /etc/tlp.conf
SATA_LINKPWR_ON_AC="max_performance"
SATA_LINKPWR_ON_BAT="med_power_with_dipm"
RADEON_POWER_PROFILE_ON_AC="high"
RADEON_POWER_PROFILE_ON_BAT="low"
RESTORE_DEVICE_STATE_ON_STARTUP="1"
EOF
else
cat << EOF > /etc/tlp.conf
SATA_LINKPWR_ON_AC="max_performance"
SATA_LINKPWR_ON_BAT="med_power_with_dipm"
RESTORE_DEVICE_STATE_ON_STARTUP="1"
EOF
fi
}

# Function to apply power saving tweaks
apply_power_saving_tweaks() {
    # Power saving tweaks code...
    echo "Applying power saving tweaks..."
    tweaks
    echo "Power saving tweaks applied!"
}

# Function to select laptop-specific tweaks
select_laptop_specific_tweaks() {
    local manufacturers=($(ls -d ../data/laptops/*))
    
    while true; do
        echo "Select the manufacturer:"
        select manufacturer in "${manufacturers[@]}"; do
            if [ -n "$manufacturer" ]; then
                select_model "$manufacturer"
                break
            else
                echo "Invalid option. Please try again."
            fi
        done
    done
}

# Function to select laptop model
select_model() {
    local manufacturer="$1"
    local models=($(ls "$manufacturer" | sed 's/\.sh$//'))
    
    while true; do
        echo "Select the laptop model or type -v to view a file in nano:"
        select model in "${models[@]}" "-v View File" "b. Back"; do
            if [ -n "$model" ]; then
                if [ "$model" == "-v" ]; then
                    view_file_nano
                elif [ "$model" == "b" ]; then
                    return
                else
                    run_laptop_tweak_script "$manufacturer/$model.sh"
                fi
            else
                echo "Invalid option. Please try again."
            fi
        done
    done
}

# Function to view a file in nano
view_file_nano() {
    read -p "Enter the file name to view in nano: " file_name
    nano "$file_name"
}

# Function to run laptop tweak script
run_laptop_tweak_script() {
    local script_path="$1"
    echo "Running $script_path..."
    ./"$script_path"  # Replace with actual script execution
    echo "$script_path executed."
}

# Main script
while true; do
    display_logo
    echo "Select the laptop tweaks you want to apply:"
    echo "1. Apply Power saving tweaks inc TLP"
    echo "2. Select Laptop specific tweaks"
    echo
    read -p "Enter option number (b to exit): " main_option

    case $main_option in
        1) apply_power_saving_tweaks ;;
        2) select_laptop_specific_tweaks ;;
        b) return ;;
        *) echo "Invalid option. Please try again." ;;
    esac
done
