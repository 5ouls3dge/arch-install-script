#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| HyprOS - Security & Performance     |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#
source global_logo.sh

echo "Installing Security & Performance tweaks (/etc/sysctl.d/99-sysctl-performance-tweaks.conf)..."

cat << EOF >/etc/sysctl.d/99-sysctl-performance-tweaks.conf

## Feature: Swappiness
## The swappiness sysctl parameter represents the kernel's preference (or avoidance) of swap space. Swappiness can have a value between 0 and 100, the default value is 60. 
## A low value causes the kernel to avoid swapping, a higher value causes the kernel to try to use swap space. Using a low value on sufficient memory is known to improve responsiveness on many systems.
vm.swappiness=10

## Feature: VFS Cache Pressure
## The value controls the tendency of the kernel to reclaim the memory which is used for caching of directory and inode objects (VFS cache). 
## Lowering it from the default value of 100 makes the kernel less inclined to reclaim VFS cache (do not set it to 0, this may produce out-of-memory conditions)
vm.vfs_cache_pressure=50

## Feature: Disable NMI Watchdog
## This action will speed up your boot and shutdown, because one less module is loaded. Additionally disabling watchdog timers increases performance and lowers power consumption
## Disable NMI watchdog
#kernel.nmi_watchdog = 0

## Feature: Dirty Ratio
## Contains, as a percentage of total available memory that contains free pages and reclaimable
## pages, the number of pages at which a process which is generating disk writes will itself start
## writing out dirty data (Default is 20).
vm.dirty_ratio = 5

## Feature: Dirty Background Ratio
## Contains, as a percentage of total available memory that contains free pages and reclaimable
## pages, the number of pages at which the background kernel flusher threads will start writing out
## dirty data (Default is 10).
vm.dirty_background_ratio = 5

## Feature: Dirty Expire Centiseconds
## This tunable is used to define when dirty data is old enough to be eligible for writeout by the
## kernel flusher threads.  It is expressed in 100'ths of a second.  Data which has been dirty
## in-memory for longer than this interval will be written out next time a flusher thread wakes up
## (Default is 3000).
#vm.dirty_expire_centisecs = 3000

## Feature: Dirty Writeback Centiseconds
## The kernel flusher threads will periodically wake up and write old data out to disk.  This
## tunable expresses the interval between those wakeups, in 100'ths of a second (Default is 500).
vm.dirty_writeback_centisecs = 1500

## Feature: Unprivileged User Namespace Clone
## Enable the sysctl setting kernel.unprivileged_userns_clone to allow normal users to run unprivileged containers.
kernel.unprivileged_userns_clone=1

## Feature: Kernel Log Hide
## To hide any kernel messages from the console
kernel.printk = 3 3 3 3

## Feature: Restrict Dmesg Access
## Restricting access to kernel logs
kernel.dmesg_restrict = 1

## Feature: Restrict Kernel Pointers
## Restricting access to kernel pointers in the proc filesystem
kernel.kptr_restrict = 2

## Feature: Disable Kexec
## Disable Kexec, which allows replacing the current running kernel. 
kernel.kexec_load_disabled = 1

## Feature: Receive Queue Size
## Increasing the size of the receive queue.
## The received frames will be stored in this queue after taking them from the ring buffer on the network card.
## Increasing this value for high speed cards may help prevent losing packets: 
net.core.netdev_max_backlog = 16384

## Feature: Max Connections
## Increase the maximum connections
##The upper limit on how many connections the kernel will accept (default 128): 
net.core.somaxconn = 8192

## Feature: Network Interface Memory
## Increase the memory dedicated to the network interfaces
## The default the Linux network stack is not configured for high speed large file transfer across WAN links (i.e. handle more network packets) and setting the correct values may save memory resources: 
net.core.rmem_default = 1048576
net.core.rmem_max = 16777216
net.core.wmem_default = 1048576
net.core.wmem_max = 16777216
net.core.optmem_max = 65536
net.ipv4.tcp_rmem = 4096 1048576 2097152
net.ipv4.tcp_wmem = 4096 65536 16777216
net.ipv4.udp_rmem_min = 8192
net.ipv4.udp_wmem_min = 8192

## Feature: TCP Fast Open
## Enable TCP Fast Open
## TCP Fast Open is an extension to the transmission control protocol (TCP) that helps reduce network latency
## by enabling data to be exchanged during the sender’s initial TCP SYN [3]. 
## Using the value 3 instead of the default 1 allows TCP Fast Open for both incoming and outgoing connections: 
net.ipv4.tcp_fastopen = 3

## Feature: BBR Congestion Control
## Enable BBR
## The BBR congestion control algorithm can help achieve higher bandwidths and lower latencies for internet traffic
net.core.default_qdisc = cake
net.ipv4.tcp_congestion_control = bbr

## Feature: TCP SYN Cookie Protection
## TCP SYN cookie protection
## Helps protect against SYN flood attacks. Only kicks in when net.ipv4.tcp_max_syn_backlog is reached: 
net.ipv4.tcp_syncookies = 1

## Feature: Time-Wait Assassination Hazard Protection
## Protect against tcp time-wait assassination hazards, drop RST packets for sockets in the time-wait state. Not widely supported outside of Linux, but conforms to RFC: 
net.ipv4.tcp_rfc1337 = 1

## Feature: Reverse Path Filtering
## By enabling reverse path filtering, the kernel will do source validation of the packets received from all the interfaces on the machine. This can protect from attackers that are using IP spoofing methods to do harm. 
net.ipv4.conf.default.rp_filter = 1
net.ipv4.conf.all.rp_filter = 1

## Feature: Disable ICMP Redirects
## Disable ICMP redirects
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv6.conf.default.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0

## Feature: FQ-PIE Queue Discipline
## To use the new FQ-PIE Queue Discipline (>= Linux 5.6) in systems with systemd (>= 217), will need to replace the default fq_codel. 
net.core.default_qdisc = fq_pie

#### Extra
if [ "$BOOTLOADER" == "grub" ]; then
## GRUB Enabling CPU Mitigations
curl https://raw.githubusercontent.com/Kicksecure/security-misc/master/etc/default/grub.d/40_cpu_mitigations.cfg -o /mnt/etc/grub.d/40_cpu_mitigations.cfg

## GRUB Distrusting the CPU
curl https://raw.githubusercontent.com/Kicksecure/security-misc/master/etc/default/grub.d/40_distrust_cpu.cfg -o /mnt/etc/grub.d/40_distrust_cpu.cfg

## GRUB Enabling IOMMU
curl https://raw.githubusercontent.com/Kicksecure/security-misc/master/etc/default/grub.d/40_enable_iommu.cfg -o /mnt/etc/grub.d/40_enable_iommu.cfg
fi

## Configure AppArmor Parser caching
sed -i 's/#write-cache/write-cache/g' /mnt/etc/apparmor/parser.conf
sed -i 's,#Include /etc/apparmor.d/,Include /etc/apparmor.d/,g' /mnt/etc/apparmor/parser.conf

## Blacklisting kernel modules
curl https://raw.githubusercontent.com/Kicksecure/security-misc/master/etc/modprobe.d/30_security-misc.conf -o /mnt/etc/modprobe.d/30_security-misc.conf
chmod 600 /mnt/etc/modprobe.d/*

## Security kernel settings.
curl https://raw.githubusercontent.com/Kicksecure/security-misc/master/usr/lib/sysctl.d/990-security-misc.conf -o /mnt/etc/sysctl.d/990-security-misc.conf
sed -i 's/kernel.yama.ptrace_scope=2/kernel.yama.ptrace_scope=3/g' /mnt/etc/sysctl.d/990-security-misc.conf
curl https://raw.githubusercontent.com/Kicksecure/security-misc/master/etc/sysctl.d/30_silent-kernel-printk.conf -o /mnt/etc/sysctl.d/30_silent-kernel-printk.conf
curl https://raw.githubusercontent.com/Kicksecure/security-misc/master/etc/sysctl.d/30_security-misc_kexec-disable.conf -o /mnt/etc/sysctl.d/30_security-misc_kexec-disable.conf
chmod 600 /mnt/etc/sysctl.d/*

## Remove nullok from system-auth
sed -i 's/nullok//g' /mnt/etc/pam.d/system-auth

## Disable coredump
echo "* hard core 0" >> /mnt/etc/security/limits.conf

EOF

# Modify system tweaks
echo "Entering HyprOS system tweaks setup..."

# Configuration file path
CONFIG_FILE="/etc/sysctl.d/99-sysctl-performance-tweaks.conf"

# Function to display the menu
display_menu() {
    clear
    echo "System tweaks available:"
    echo "--------------"
    awk -F'#' '/^## Feature:/{print $3 "\n##--------------"; next} /^[0-1] #/{print $0 " [ENABLED]/[DISABLED]"}' "$CONFIG_FILE"
    echo "--------------"
    echo "Select an option:"
    echo "  1-$(awk -F'#' '/^## Feature:/{print $3}' "$CONFIG_FILE" | wc -l): Enable/Disable a feature"
    echo "  -h[1-9]: Get help for a feature option"
    echo "  0: Exit"
}

# Function to enable or disable a feature
toggle_feature() {
    read -p "Enter the number of the feature to toggle (0 to exit, -h for help): " choice
    case $choice in
        0)
            exit
            ;;
        -h[1-9]|[1-9])
            feature_number=$(echo "$choice" | cut -c 3-)
            awk -F'#' -v num="$feature_number" '/^## Feature:/{if (num==$2) print $3 "\n##--------------"; next} /^[0-1] #/{if (num==$2) print $3}' "$CONFIG_FILE"
            ;;
        [1-9])
            sed -i "${choice}s/^\([0-9]\) #/\1 #/" "$CONFIG_FILE"  # Enable the option
            ;;
        *)
            echo "Invalid option. Please enter a number between 0 and $(awk -F'#' '/^## Feature:/{print $3}' "$CONFIG_FILE" | wc -l), -h followed by the feature number for help, or 0 to exit."
            ;;
    esac
}

# Install the extra mods.
install_tweaks() {
    CONFIG_FILE="/etc/sysctl.d/99-sysctl-performance-tweaks.conf"
    TEMP_SCRIPT="temp_t.sh"

    # Check if the file exists and has content
    if [ -s "$CONFIG_FILE" ]; then
        # Copy lines from "#### Extra" onwards to temporary script
        awk '/^#### Extra$/,0' "$CONFIG_FILE" > "$TEMP_SCRIPT"

        # Modify the temporary script to add shebang at the top
        sed -i '1s/^/#!\/bin\/bash\n/' "$TEMP_SCRIPT"

        # Remove the copied lines from the original file
        awk '!/^#### Extra$/' "$CONFIG_FILE" > "$CONFIG_FILE.temp"
        mv "$CONFIG_FILE.temp" "$CONFIG_FILE"

        # Make the temporary script executable
        chmod +x "$TEMP_SCRIPT"

        # Execute the temporary script
        ./"$TEMP_SCRIPT"

        # Delete the temporary script
        rm "$TEMP_SCRIPT"

        echo "Selected tweaks installed."
    else
        echo "Error: $CONFIG_FILE does not exist or is empty."
    fi
}

# Main loop
while true; do
    display_menu
    toggle_feature
done
