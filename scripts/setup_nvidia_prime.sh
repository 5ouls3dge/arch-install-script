#!/bin/bash
#|---/ /+--------------------------+---/ /|#
#|--/ /-| Prime apps seletion      |--/ /-|#
#|-/ /--| # by 5ouls3edge (2024)   |-/ /--|#
#|/ /---+--------------------------+/ /---|#

# Show apps from ../data/packages/prime_apps.lst to run with prime-run or enter custom package name
add_app_to_prime() {
    clear
    display_logo
    echo "Choose a category to manage apps to run on your dedicated Nvidia GPU:"
    categories=($(ls ../data/packages/ | sed 's/\.lst//' | awk '{print toupper(substr($0, 1, 1)) tolower(substr($0, 2))}'))
    select_category "${categories[@]}"
}

# Select an app to toggle
select_category() {
    PS3="Select a category to manage (1 - ${#categories[@]}, b for back): "
    select cat_choice in "${categories[@]}" "Back"; do
        if [ "$cat_choice" == "Back" ]; then
            main_menu
        elif [[ $REPLY -ge 1 && $REPLY -le ${#categories[@]} ]]; then
            selected_category="$cat_choice"
            list_apps_in_category
        else
            echo "Invalid choice. Please enter a number between 1 and ${#categories[@]} or 'b' for back."
        fi
    done
}

# Function to list apps in the selected category
list_apps_in_category() {
    clear
    display_logo
    echo "Select apps to add to prime-run in the category '$selected_category':"
    apps=($(awk '{print $1}' "../data/packages/$selected_category.lst"))
    select_app_to_add "${apps[@]}"
}

# Function to select apps to add to prime-run
select_app_to_add() {
    PS3="Select apps to add to prime-run (1 - ${#apps[@]}, b for back): "
    select app_choice in "${apps[@]}" "Back"; do
        if [ "$app_choice" == "Back" ]; then
            add_app_to_prime
        elif [[ $REPLY -ge 1 && $REPLY -le ${#apps[@]} ]]; then
            add_to_prime_run "$app_choice"
        else
            echo "Invalid choice. Please enter a number between 1 and ${#apps[@]} or 'b' for back."
        fi
    done
}

# Function to add an app to prime_run_apps.lst
add_to_prime_run() {
    app_name="$1"
    echo "Adding $app_name to prime-run..."
    echo "$app_name prime-run $app_name" >> ../data/prime_run_apps.lst
    echo "App added successfully!"
    sleep 2
    list_apps_in_category
}

# Function to remove an app from prime_run_apps.lst
remove_from_prime_run() {
    clear
    display_logo
    echo "Current apps which are offloaded to Nvidia GPU:"
    awk '{print NR, $0}' ../data/prime_run_apps.lst | column -t -s' '
    echo "Choose apps to remove from prime-run:"
    IFS=$'\n' read -r -d '' -a prime_apps < ../data/prime_run_apps.lst
    PS3="Select apps to remove from prime-run (1 - ${#prime_apps[@]}, b for back): "
    select remove_choice in "${prime_apps[@]}" "Back"; do
        if [ "$remove_choice" == "Back" ]; then
            main_menu
        elif [[ $REPLY -ge 1 && $REPLY -le ${#prime_apps[@]} ]]; then
            remove_from_list "$remove_choice"
        else
            echo "Invalid choice. Please enter a number between 1 and ${#prime_apps[@]} or 'b' for back."
        fi
    done
}

# Function to remove an app from prime_run_apps.lst
remove_from_list() {
    app_name=$(echo "$1" | awk '{print $1}')
    echo "Removing $app_name from prime-run..."
    sed -i "/$app_name/d" ../data/prime_run_apps.lst
    echo "App removed successfully!"
    sleep 2
    remove_from_prime_run
}

# Function for the main menu
main_menu() {
    clear
    display_logo
    echo "Current apps which are offloaded to Nvidia dGPU:"
    awk '{print NR, toupper(substr($0, 1, 1)) tolower(substr($0, 2))}' ../data/prime_run_apps.lst | column -t -s' '
    PS3="Choose from the following (1 - 3, q to quit): "
    options=("Add app to run on Prime" "Remove app from Prime" "Exit")
    select opt in "${options[@]}"; do
        case $opt in
            "Add app to run on Prime")
                add_app_to_prime
                ;;
            "Remove app from Prime")
                remove_from_prime_run
                ;;
            "Exit")
                return
                ;;
            *)
                echo "Invalid choice. Please enter a number between 1 and ${#options[@]} or 'q' to quit."
                ;;
        esac
    done
}

# Start the main menu
main_menu
