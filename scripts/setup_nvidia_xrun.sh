#!/bin/bash
#|---/ /+--------------------------------+---/ /|#
#|--/ /-| Nvidia XRun apps seletion      |--/ /-|#
#|-/ /--| # by 5ouls3edge (2024)         |-/ /--|#
#|/ /---+--------------------------------+/ /---|#

read -r -p "would you like to install and setup Nvidia xrun apps (this can also be done in the HyprOS setting app later)" "set_xrun"


xrun_setup() {
    
}

if [ "$set_xrun" == "y" ]; then
    echo "Installing xrun"
    xrun_setup
fi

if [ -n !"$set_xrun"]; then
    echo "Skipping Xrun installation..."
fi