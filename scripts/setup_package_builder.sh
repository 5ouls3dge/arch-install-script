#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| Build combined_apps.lst             |--/ /-|#
#|-/ /--| # by 5ouls3dge (2024)               |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

# Define paths
data_dir="../data/packages"
output_file="combined_apps.lst"

# Package lists to combine
system_list="system.lst"
dev_list="development.lst"
prod_list="productivity.lst"
media_list="media.lst"
games_list="games.lst"

# Check if the necessary files and directories exist
if [ ! -d "$data_dir" ]; then
    echo "Error: Subdirectory 'apps' does not exist."
    exit 1
fi

if [ ! -f "$build_script" ]; then
    echo "Error: Build script 'build_apps.sh' not found in the 'apps' directory."
    exit 1
fi

if [ ! -f "$data_dir/$system_list" ] || [ ! -f "$data_dir/$dev_list" ] || [ ! -f "$data_dir/$prod_list" ] || [ ! -f "$data_dir/$media_list" ] || [ ! -f "$data_dir/$games_list" ]; then
    echo "Error: One or more required list files not found in the 'apps' directory."
    exit 1
fi

# Wipe or create combined_apps.lst
if [ -f "$data_dir/$output_file" ]; then
    > "$data_dir/$output_file"  # Wipe the file if it exists
else
    touch "$data_dir/$output_file"  # Create the file if it doesn't exist
fi

# Function to combine all lists
combine_lists() {
    cat "$data_dir/$system_list" "$data_dir/$dev_list" "$data_dir/$prod_list" "$data_dir/$media_list" "$data_dir/$games_list" | grep -vE '^\s*#|^$'
}

# Execute combine_lists function to generate content
content=$(combine_lists)

# Append content to combined_apps.lst
echo "$content" >> "$data_dir/$output_file"

echo "Combined apps list updated successfully."
