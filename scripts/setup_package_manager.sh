#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| Package Manager                     |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#
source global_logo.sh
main_lst_file="lists/package_lists/combined_apps.lst"

# Function to display the main menu
display_main_menu() {
    display_logo
    echo "Select an option below."
    echo "Important: Once you finish in the Package Manager, you must run Option 2 to install packages:"
    echo
    echo "1. Edit Package Manager"
    echo "2. Install apps now and exit"
    echo "3. Delete the generated file 'combined_apps.lst'"
    echo "4. Exit without installing apps"
    echo
}

# Display the Package Manager menu
display_app_store_menu() {
    display_logo
    echo "Package Manager - Choose a category:"
    echo
    categories=($(ls "lists/package_lists/" | grep -v 'combined_apps.lst' | sed 's/\.lst//'))
    for ((i=0; i<${#categories[@]}; i++)); do
        echo "$(($i+1)). ${categories[i]}"
    done
    echo "b. Back"
}

# Delete package from list menu
display_category_menu() {
    clear
    display_logo
    category="$1"
    lst_file="lists/package_lists/${category}.lst"

    # Display all packages, even disabled ones
    echo "Current packages in $category:"
    echo
    awk '{ printf "%-3s%-25s%s\n", NR".", $2, ($1 ~ /^#/) ? "(disabled)" : "" }' "$lst_file"

    echo
    echo "Select an action below:"
    echo
    echo "1. Delete package from list"
    echo "2. Back to Package Manager"
    
    echo
    read -p "Enter your choice (1-2, b: Back): " category_choice

    case $category_choice in
        1) delete_package "$lst_file"; display_category_menu "$category" ;;
        2) return ;;
        *) echo "Invalid choice. Please enter a number between 1 and 2, or 'b' for Back." ;;
    esac
}

# Delete a package from a category
delete_package() {
    lst_file="$1"
    
    clear
    display_logo
    echo "Current packages in $lst_file:"
    echo
    awk '{ print NR, $0 }' "$lst_file" | pr -t -4
    
    echo
    read -p "Enter the number(s) of the package(s) to delete (e.g. 4 8 14): " delete_numbers

    # Convert delete_numbers into an array
    read -a delete_array <<< "$delete_numbers"

    # Variable to track whether any packages were deleted
    deleted=false

    # Iterate over the array to delete the specified packages
    for delete_num in "${delete_array[@]}"; do
        # Use sed to delete the specified line
        sed -i "${delete_num}d" "$lst_file" && deleted=true
    done

    if [ "$deleted" = true ]; then
        echo
        echo "Selected packages deleted."
    else
        echo
        echo "No packages were deleted. Please enter valid package numbers."
    fi
}

# Toggle packages in a category
add_remove_packages() {
    lst_file="$1"
    clear
    display_logo
    echo "Current packages available to add/remove in $lst_file:"
    echo
    awk '/^\s*#/' "$lst_file" | nl -w 3 -s " " -b a

    echo
    read -p "Select package(s) to add or press 'n' to enter new package(s): " input_packages

    if [ "$input_packages" = "n" ]; then
        echo
        read -p "Enter the names of the package(s) you wish to add to list (e.g., postgresql gimp-git apparmor snapper blender-4.1-bin): " custom_package_names
        echo "$custom_package_names" >> "$lst_file"
        echo "Custom package(s) \"$custom_package_names\" added."
    else
        # Convert input_packages into an array
        read -a packages_array <<< "$input_packages"

        # Iterate over the array to check for duplicates and add to the file
        for package_name in "${packages_array[@]}"; do
            if grep -q "^#.*$package_name" "$lst_file"; then
                # Remove if the package already exists in the list
                sed -i "/^#.*$package_name/d" "$lst_file"
                echo "Package \"$package_name\" removed."
            fi
            echo "$package_name" >> "$lst_file"
        done
        echo
        echo "Packages added to list."
    fi
}

# Function to delete combined_apps.lst
delete_combined_apps_lst() {
    clear
    display_logo
    read -p "Are you sure you want to delete 'combined_apps.lst'? (y/n): " delete_choice
    echo
    case $delete_choice in
        [Yy])
            rm "$main_lst_file"
            echo "'combined_apps.lst' deleted."
            ;;
        [Nn])
            echo "Deletion canceled."
            ;;
        *)
            echo "Invalid choice. Please enter 'y' or 'n'."
            ;;
    esac
}

# Function to install packages from combined_apps.lst  using yay and exit
install_packages_and_exit() {
    clear
    display_logo
    echo "Building combined_apps.sh..."
    ./setup_package_builder.sh
    echo "Installing apps from 'combined_apps.lst' using yay..." 
    echo  
    # Ensure yay is installed
    if ! command -v yay &> /dev/null; then
        echo "yay is not installed. Installing yay..."
        sudo pacman -S --noconfirm yay
        if [ $? -ne 0 ]; then
            echo "Failed to install yay. Exiting."
            exit 1
        fi
    fi
    
    # Install apps listed in combined_apps.lst using yay
    yay -S --noconfirm $(< "$main_lst_file")

    # Check the exit status of the yay command
    if [ $? -eq 0 ]; then
        echo "Apps installed successfully. Exiting..."
        exit 0
    else
        echo "Failed to install some or all apps. Please check and try again."
        exit 1
    fi
}

# Main loop
while true; do
    display_main_menu

    read -p "Enter your choice (1-4): " main_choice
    echo
    case $main_choice in
        1) 
            while true; do
                display_app_store_menu
                echo
                read -p "Enter your choice (1-$((${#categories[@]}+1)), b: Back): " app_store_choice
                case $app_store_choice in
                    [1-9]|[1-9][0-9])
                        selected_category="${categories[$(($app_store_choice-1))]}"
                        display_category_menu "$selected_category"
                        ;;
                    b)
                        break
                        ;;
                    *)
                        echo "Invalid choice. Please enter a number between 1 and $((${#categories[@]}+1)) or 'b' for Back."
                        ;;
                esac
            done
            ;;
        2) install_packages_and_exit ;;
        3) delete_combined_apps_lst ;;
        4) echo "Exiting."; break ;;
        *) echo "Invalid choice. Please enter a number between 1 and 4." ;;
    esac
done
