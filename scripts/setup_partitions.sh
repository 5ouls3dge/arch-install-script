#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| Partition Setup                     |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

# formatting the disk
wipefs -af "$DISK" &>/dev/null
sgdisk -Zo "$DISK" &>/dev/null

# Partition disks
sgdisk --clear \
       --new=1:0:+512M --typecode=1:ef00 --change-name=1:ESP \
       --new=2:0:0     --typecode=2:8300 --change-name=2:LUKS \
       --align=2048 $DEVICE

# Informing the Kernel of the changes.
echo "Informing the Kernel about the disk changes."
partprobe "$DISK"umount $MOUNT_POINT

# Set up LUKS
echo "Creating the encrypted partition."
echo -n "Enter encryption passphrase: "
cryptsetup luksFormat --type luks2 $OSROOT
echo -n "Enter encryption passphrase again: "
cryptsetup open $OSROOT luksroot

# Format ESP
mkfs.vfat -F32 -n ESP $EFI

# Format the LUKS volume as Btrfs
$ mkfs.btrfs -f -L system $BTRFS
mount -o noatime,compress=zstd:2 $BTRFS $MOUNT_POINT

# Create Btrfs subvolumes and set COW
mount -t btrfs $BTRFS $MOUNT_POINT
btrfs subvolume create $MOUNT_POINT/@ &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/snapshots &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/boot/ &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/home &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/root &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/srv &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_log &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_log_journal &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_crash &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_cache &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_tmp &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_spool &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_lib_libvirt_images &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_lib_machines &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_lib_sddm &>/dev/null
btrfs subvolume create $MOUNT_POINT/@/var_lib_AccountsService &>/dev/null

# Disable COW on subvolumes
chattr +C $MOUNT_POINT/@/boot
chattr +C $MOUNT_POINT/@/srv
chattr +C $MOUNT_POINT/@/var_log
chattr +C $MOUNT_POINT/@/var_log_journal
chattr +C $MOUNT_POINT/@/var_crash
chattr +C $MOUNT_POINT/@/var_cache
chattr +C $MOUNT_POINT/@/var_tmp
chattr +C $MOUNT_POINT/@/var_spool
chattr +C $MOUNT_POINT/@/var_lib_libvirt_images
chattr +C $MOUNT_POINT/@/var_lib_machines
chattr +C $MOUNT_POINT/@/var_lib_sddm
chattr +C $MOUNT_POINT/@/var_lib_AccountsService

#Set the default BTRFS Subvol to Snapshot 1 before pacstrapping
SNAPSHOT_ID="$(btrfs subvolume list $MOUNT_POINT | grep "/@/snapshots/1/snapshot" | grep -oP '(?<=ID )[0-9]+')"
btrfs subvolume set-default "$SNAPSHOT_ID" $MOUNT_POINT

cat << EOF >> $MOUNT_POINT/@snapshots/1/snapshot/info.xml
<?xml version="1.0"?>
<snapshot>
  <type>single</type>
  <num>1</num>
  <date>1999-03-31 0:00:00</date>
  <description>First Root Filesystem</description>
  <cleanup>number</cleanup>
</snapshot>
EOF
chmod 600 $MOUNT_POINT/@snapshots/1/snapshot/info.xml

# Mount the subvolumes
umount $MOUNT_POINT
echo "Mounting the newly created subvolumes."
mount -o subvol=@ $BTRFS $MOUNT_POINT
mkdir -p $MOUNT_POINT/{home,snapshots}
mount -o subvol=@home $BTRFS $MOUNT_POINT/home
mount -o subvol=@snapshots $BTRFS $MOUNT_POINT/snapshots

mount -o ssd,noatime,space_cache,compress=zstd:15 $BTRFS /mnt
mkdir -p /mnt/{boot,root,home,snapshots,srv,tmp,/var/log,/var/crash,/var/cache,/var/tmp,/var/spool,/var/lib/libvirt/images,/var/lib/machines,/var/lib/sddm,/var/lib/AccountsService}
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodev,nosuid,noexec,subvol=@/boot $BTRFS /mnt/boot
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodev,nosuid,subvol=@/root $BTRFS /mnt/root
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodev,nosuid,subvol=@/home $BTRFS /mnt/home
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,subvol=@/snapshots $BTRFS /mnt/snapshots
mount -o ssd,noatime,space_cache=v2.autodefrag,compress=zstd:15,discard=async,subvol=@/srv $BTRFS /mnt/srv
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodatacow,nodev,nosuid,noexec,subvol=@/var_log $BTRFS /mnt/var/log

# Toolbox (https://github.com/containers/toolbox) needs /var/log/journal to have dev, suid, and exec, Thus I am splitting the subvolume. Need to make the directory after /mnt/var/log/ has been mounted.
mkdir -p /mnt/var/log/journal
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodatacow,subvol=@/var_log_journal $BTRFS /mnt/var/log/journal

mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodatacow,nodev,nosuid,noexec,subvol=@/var_crash $BTRFS /mnt/var/crash
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodatacow,nodev,nosuid,noexec,subvol=@/var_cache $BTRFS /mnt/var/cache
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodatacow,nodev,nosuid,noexec,subvol=@/var_tmp $BTRFS /mnt/var/tmp

mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodatacow,nodev,nosuid,noexec,subvol=@/var_spool $BTRFS /mnt/var/spool
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodatacow,nodev,nosuid,noexec,subvol=@/var_lib_libvirt_images $BTRFS /mnt/var/lib/libvirt/images
mount -o ssd,noatime,space_cache=v2,autodefrag,compress=zstd:15,discard=async,nodatacow,nodev,nosuid,noexec,subvol=@/var_lib_machines $BTRFS /mnt/var/lib/machines

mkdir -p /mnt/boot/efi
mount -o nodev,nosuid,noexec $EFI /mnt/boot/efi
