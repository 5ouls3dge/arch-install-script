#!/bin/bash
#|---/ /+--------------------------+---/ /|#
#|--/ /-| Snapper Timeline setup   |--/ /-|#
#|-/ /--| # by 5ouls3edge (2024)   |-/ /--|#
#|/ /---+--------------------------+/ /---|#

## SNAPPER ##
# Snapper configuration
umount /snapshots
rm -r /snapshots
snapper --no-dbus -c root create-config /
btrfs subvolume delete /snapshots
mkdir /snapshots
mount -a
chmod 750 /snapshots
chmod a+rx /.snapshots
chown :wheel /.snapshots
snapper -c root create --description "Fresh Install"

# Function to prompt user for a time limit value
echo "Set the time limits for snapper snapshots:"
echo
echo "TIMELINE_MIN_AGE=\"1800\" - Minimum age of snapshots is set to 30 minutes."
echo "TIMELINE_LIMIT_HOURLY=\"10\" - Retains the last 10 snapshots."
echo "TIMELINE_LIMIT_DAILY=\"10\" - The first daily snapshot that has been taken is retained for 10 days."
echo "TIMELINE_LIMIT_WEEKLY=\"10\" - The first weekly snapshot is retained for 10 weeks."
echo "TIMELINE_LIMIT_MONTHLY=\"10\" - The first monthly snapshot is retained for 10 months."
echo "TIMELINE_LIMIT_YEARLY=\"10\" - The first snapshot taken on the last day of the year is retained for 10 years."
echo

prompt_for_limit() {
    read -p "Enter the value for $1 (eg 10): " value
    echo "$value"
}

# Set snapper time limits to keep snapshots
TIMELINE_MIN_AGE=$(prompt_for_limit "TIMELINE_MIN_AGE")
TIMELINE_LIMIT_HOURLY=$(prompt_for_limit "TIMELINE_LIMIT_HOURLY")
TIMELINE_LIMIT_DAILY=$(prompt_for_limit "TIMELINE_LIMIT_DAILY")
TIMELINE_LIMIT_WEEKLY=$(prompt_for_limit "TIMELINE_LIMIT_WEEKLY")
TIMELINE_LIMIT_MONTHLY=$(prompt_for_limit "TIMELINE_LIMIT_MONTHLY")
TIMELINE_LIMIT_YEARLY=$(prompt_for_limit "TIMELINE_LIMIT_YEARLY")

# Update snapper configuration
sed -i "s/^TIMELINE_MIN_AGE.*/TIMELINE_MIN_AGE=\"$TIMELINE_MIN_AGE\"/" /etc/snapper/configs/root &&
sed -i "s/^TIMELINE_LIMIT_HOURLY.*/TIMELINE_LIMIT_HOURLY=\"$TIMELINE_LIMIT_HOURLY\"/" /etc/snapper/configs/root &&
sed -i "s/^TIMELINE_LIMIT_DAILY.*/TIMELINE_LIMIT_DAILY=\"$TIMELINE_LIMIT_DAILY\"/" /etc/snapper/configs/root &&
sed -i "s/^TIMELINE_LIMIT_WEEKLY.*/TIMELINE_LIMIT_WEEKLY=\"$TIMELINE_LIMIT_WEEKLY\"/" /etc/snapper/configs/root &&
sed -i "s/^TIMELINE_LIMIT_MONTHLY.*/TIMELINE_LIMIT_MONTHLY=\"$TIMELINE_LIMIT_MONTHLY\"/" /etc/snapper/configs/root &&
sed -i "s/^TIMELINE_LIMIT_YEARLY.*/TIMELINE_LIMIT_YEARLY=\"$TIMELINE_LIMIT_YEARLY\"/" /etc/snapper/configs/root

echo "Snapper TIMELINE LIMITS updated successfully."
