# Generating /etc/fstab.
echo "Generating a new fstab."
genfstab -U -p $MOUNT_POINT >> $MOUNT_POINT/etc/fstab
sed -i 's#,subvolid=258,subvol=/@snapshots/1/snapshot,subvol=@snapshots/1/snapshot##g' /mnt/etc/fstab

# Setting hostname.
echo "$HOSTNAME" > $MOUNT_POINT/etc/hostname

# Setting hosts file.
echo "Setting hosts file."
cat > /mnt/etc/hosts <<EOF
127.0.0.1   localhost
::1         localhost
127.0.1.1   $HOSTNAME.localdomain   $HOSTNAME
EOF

# Setting up locales.
echo "$LOCALE.UTF-8 UTF-8"  > /mnt/etc/locale.gen
echo "LANG=$LOCALE.UTF-8" > /mnt/etc/locale.conf

# Setting up keyboard layout.
read -r -p "Please insert the keyboard layout you use: " kblayout
echo "KEYMAP=$KBLAYOUT" > /mnt/etc/vconsole.conf

# Configuring /etc/mkinitcpio.conf
echo "Configuring /etc/mkinitcpio for ZSTD compression and LUKS hook."
sed -i 's,#COMPRESSION="zstd",COMPRESSION="zstd",g' /mnt/etc/mkinitcpio.conf
sed -i 's,modconf block filesystems keyboard,keyboard modconf block encrypt filesystems,g' /mnt/etc/mkinitcpio.conf
