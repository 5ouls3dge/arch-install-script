#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| Systemctl commands                  |--/ /-|#
#|-/ /--| by 5ouls3dge (2024)                 |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

# Change audit logging group
echo "log_group = audit" >> /mnt/etc/audit/auditd.conf

# Enabling audit service.
systemctl enable auditd --root=/mnt &>/dev/null

systemctl enable snapper-timeline.timer --root=/mnt &>/dev/null
systemctl enable snapper-cleanup.timer --root=/mnt &>/dev/null

# Disable and stop systemd-timesyncd service
systemctl disable --now --root=/mnt &>/dev/null systemd-timesyncd.service

# Mask systemd-rfkill socket and service
systemctl mask --root=/mnt &>/dev/null systemd-rfkill.socket systemd-rfkill.service

# Enable NetworkManager
systemctl enable --root=/mnt &>/dev/null NetworkManager

# Enable NetworkManager-wait-online
systemctl enable --root=/mnt &>/dev/null NetworkManager-wait-online

# Enable NetworkManager-dispatcher
systemctl enable --root=/mnt &>/dev/null NetworkManager-dispatcher

# Enable and start nftables
systemctl enable --root=/mnt &>/dev/null nftables

# Enable opennic-up timer
systemctl enable --root=/mnt &>/dev/null opennic-up.timer

# Enable sshd
systemctl enable --root=/mnt &>/dev/null sshd

# Enable chronyd
systemctl enable --root=/mnt &>/dev/null chronyd

# Enable reflector
systemctl enable --root=/mnt &>/dev/null reflector

# Enabling Reflector timer.
systemctl enable --root=/mnt &>/dev/null reflector.timer

# Enable apparmor
systemctl enable --root=/mnt &>/dev/null apparmor

# Enable sshguard
systemctl enable --root=/mnt &>/dev/null sshguard

# Enable tlp 
systemctl enable --root=/mnt &>/dev/null tlp

# Enable memavaild 
systemctl enable --root=/mnt &>/dev/null memavaild

# Enable haveged 
systemctl enable --root=/mnt &>/dev/null haveged

# Enable irqbalance 
systemctl enable --root=/mnt &>/dev/null irqbalance

# Enable prelockd 
systemctl enable --root=/mnt &>/dev/null prelockd

# Enable nohang-desktop 
systemctl enable --root=/mnt &>/dev/null nohang-desktop

# Enable auto-cpufreq 
systemctl enable --root=/mnt &>/dev/null auto-cpufreq

# Enable dbus-broker
systemctl enable --root=/mnt &>/dev/null dbus-broker

# Enable postgresql
systemctl enable --root=/mnt &>/dev/null postgresql

# Start psd service for user
systemctl --user --root=/mnt &>/dev/null start psd

# Enabling auto-trimming service.
systemctl enable --root=/mnt &>/dev/null fstrim.timer

# Enabling Firewalld.
systemctl enable --root=/mnt &>/dev/null firewalld

# Enabling systemd-oomd.
echo "Enabling systemd-oomd."
systemctl enable --root=/mnt &>/dev/null systemd-oomd

# Enable restic
sudo systemctl enable --now restic-backup.service

# Enabling Snapper automatic snapshots.
echo "Enabling Snapper and automatic snapshots entries."
