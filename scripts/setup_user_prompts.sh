#!/bin/bash
#|---/ /+-------------------------------------+---/ /|#
#|--/ /-| User prompt for config Script       |--/ /-|#
#|-/ /--| # by 5ouls3dge (2024)               |-/ /--|#
#|/ /---+-------------------------------------+/ /---|#

# Selecting the kernel flavor to install.
kernel_selector () {
    echo "List of kernels:"
    echo "1) Stable — Vanilla Linux kernel and modules, with a few patches applied."
    echo "2) Hardened — A security-focused Linux kernel."
    echo "3) Longterm — Long-term support (LTS) Linux kernel and modules."
    echo "4) Zen Kernel — Optimized for desktop usage."
    echo
    read -r -p "Insert the number of the corresponding kernel: " choice
    echo
    echo "$choice will be installed"
    case $choice in
        1 ) kernel=linux
            ;;
        2 ) kernel=linux-hardened
            ;;
        3 ) kernel=linux-lts
            ;;
        4 ) kernel=linux-zen
            ;;
        * ) echo "You did not enter a valid selection."
            kernel_selector
    esac
}

# Selecting the target for the installation.
PS3="Select the disk where Linux is going to be installed: "
echo
select ENTRY in $(lsblk -dpnoNAME|grep -P "/dev/sd|nvme|vd");
do
    DISK=$ENTRY
    echo
    echo "Installing Arch Linux on $DISK."
    break
done

# Confirming the disk selection.
echo
read -r -p "This will delete the current partition table on $DISK. Do you agree [y/N]? " response
response=${response,,}
if [[ ! ("$response" =~ ^(yes|y)$) ]]; then
    echo "Quitting."
    exit
fi

# Select kernel
kernel_selector

# Check GPU vendors and set variable
if lspci | grep -i "NVIDIA" &> /dev/null; then
    set_gpu="NVIDIA"
elif lspci | grep -i "AMD" &> /dev/null; then
    set_gpu="AMD"
fi

# Check and set the microcode to install.
CPU=$(grep vendor_id /proc/cpuinfo)
if [[ $CPU == *"AuthenticAMD"* ]]; then
    microcode=amd-ucode
else
    microcode=intel-ucode
fi

# Use defined varibals set in global_set_varibles? if no you will be prompted through the install options.
while true; do
    read -p "Use defined variables set in global_set_variables? (y/n): " choice

    # Validate the input
    if [[ "$choice" == "y" ]]; then
        echo "Run using defined variables."
        break
    elif [[ "$choice" == "n" ]]; then
        echo "Hello"
        break
    else
        echo "Invalid choice. Please enter 'y' or 'n'."
    fi
done

user_prompts() {
    # Select bootloader
    # Define options for bootloader selection
    options=("No Bootloader + SecureBoot" "Cancel")

    echo "Select bootloader:"
    select bootloader_option in "${options[@]}"; do
        case $bootloader_option in
            "No Bootloader + SecureBoot")
                set_bootloader="secureboot"
                break
                ;;
            "Cancel")
                echo "Operation canceled."
                return
                ;;
            *)
                echo "Invalid option. Please select a valid option."
                ;;
        esac
    done

    # Setting username
    read -r -p "Please enter name for a user account (leave empty to skip): " set_username

    # Setting password.
    if [[ -n $username ]]; then
        while true; do
            read -s -r -p "Please enter a password for the user account: " password1
            echo
            read -s -r -p "Please confirm the password: " password2
            echo
            if [[ $password1 == $password2 ]]; then
                set_password=$password1
                break
            else
                echo "Passwords do not match. Please try again."
            fi
        done
    fi

    # Set locales.
    while true; do
        read -r -p "Set the locale to use (eg. en_GB, en_US, de_DE, fr_FR, zh_CN): " set_locale
        if [[ ${set_locale:0:2} == [a-z][a-z] && ${set_locale: -2} == [A-Z][A-Z] && ${set_locale:2:1} == "_" ]]; then
            echo "Locale '$set_locale' is valid."
            break
        else
            echo "Invalid locale format. Please enter a valid locale (eg. en_GB, en_US, de_DE, fr_FR, zh_CN)."
        fi
    done

    # Select Keyboard
    # Retrieve available keyboard layouts
    get_available_layouts() {
        localectl list-keymaps | grep -oP "^($set_kblayout)"
    }

    # Check if the layout is valid
    check_layout() {
        local layout=$1
        if [[ "${available_layouts[*]}" =~ "$layout" ]]; then
            return 0  # Layout is valid
        else
            return 1  # Layout is invalid
        fi
    }

    # Get available keyboard layouts
    available_layouts=($(get_available_layouts))

    # Prompt for the keyboard layout
    while true; do
        read -r -p "Set the keyboard layout you use (eg. uk, us, de, fr, ru): " set_kblayout
        if check_layout "$set_kblayout"; then
            echo "Keyboard layout '$set_kblayout' is valid."
            break
        else
            echo "Invalid keyboard layout. Please enter a valid layout (eg. uk, us, de, fr, ru)."
        fi
    done

    # Validate hostname
    validate_hostname() {
        local hostname=$1
        local regex="^[a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?(\.[a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?)*$"
        if [[ $hostname =~ $regex ]]; then
            echo "Hostname '$hostname' is valid."
            return 0
        else
            echo "Invalid hostname. Please enter a valid hostname."
            return 1
        fi
    }

    # Prompt to enter hostname
    while true; do
        read -r -p "Please enter the hostname: " check_hostname
        if validate_hostname "$check_hostname"; then
            break
        fi
    done

    # Convert hostname to lowercase
    set_hostname=$(echo "$check_hostname" | tr '[:upper:]' '[:lower:]')

    # Install VM and CONFIGURE.
    while true; do
        read -r -p "Install Virtual Machine (y/n): " set_vm
        case $set_vm in
            [Yy]*) break ;;
            [Nn]*) break ;;
            *) echo "Please enter 'y' for yes or 'n' for no." ;;
        esac
    done

    # Install Docker.
    while true; do
        read -r -p "Install Docker (y/n): " set_docker
        case $set_docker in
            [Yy]*) break ;;
            [Nn]*) break ;;
            *) echo "Please enter 'y' for yes or 'n' for no." ;;
        esac
    done

    # Install Restic
    while true: do
        read -p "Install Restic: " set_restic
        case $set_restic in
            [Yy]*) break;;
            [Nn]*) break;;
            *) echo "Please enter 'y' for yes or 'n' for no." ;;
        esac
    done

    # Restic local or remote backup?
    if [ "$set_restic" == "y" || "$set_restic" == "Y" ]; then
        while true; do
            read -p "Backup data to remote server(r) or local drive(l): " set_backup_device
            # Check the validity of the input using a case statement
            case "$set_backup_device" in 
                r) perform_remote_backup; break ;; 
                l) perform_local_backup; break ;; 
                *) echo "Invalid input. 
                Please enter 'r' for remote server or 'l' for local drive:" ;; 
            esac
        done
    fi
}

# Use pre-defined varibals set in global_set_varibles? if no you will be prompted through the install options.
while true; do
    read -p "Use defined variables set in global_set_variables? (y/n) If (n) is selected you will go through each question: " choice

    # Validate the input
    if [[ "$choice" == "y" ]]; then
        ./global_set_variables.sh
        break
    elif [[ "$choice" == "n" ]]; then
        user_prompts
        break
    else
        echo "Invalid choice. Please enter 'y' or 'n'."
    fi
done

# Generated variables from prompts
export GPU="${set_gpu}"
export DEVICE="${DISK}"
export EFI="${DEVICE}1"
export OSROOT="${DEVICE}2"
export BTRFS="/dev/mapper/luksroot"
export MOUNT_POINT="/mnt"
export BOOTLOADER="$set_bootloader"
export USERNAME="${set_username}"
export PASSWORD="${set_password}"
export HOSTNAME="${set_hostname}"
export LOCALE="${set_locale}"
export KBLAYOUT="${set_kblayout}"
export VMINSTALL="${set_vm}"
export DOCKERINSTALL="${set_docker}"
export RESTICINSTALL="${set_backup_device}"
export BACKUPDESTINATION=${set_backup_device}
export ISLAPTOP="${set_laptop}"
